from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(label="Name", required=True, max_length=128)
    email = forms.EmailField(label="Email", required=True)
    subject = forms.CharField(label="Subject", max_length=256)
    message = forms.CharField(label="Message", required=True, widget=forms.Textarea)

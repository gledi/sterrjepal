from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render, redirect

from .forms import ContactForm


def about_us(request):
    return render(request, "pages/about_us.html")


def contact_us(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            email = form.cleaned_data["email"]
            subject = form.cleaned_data.get("subject", "Greetings from your website!")
            message = form.cleaned_data["message"]

            send_mail(
                subject,
                f"FROM: {name}\n{message}",
                email,
                settings.ADMINS,
                fail_silently=False,
                html_message=f"<div><p><em>From:</em> {name}</p><p>{message}</p></div>",
            )
            return redirect("pages:contact_us")
    else:
        form = ContactForm()
    return render(request, "pages/contact_us.html", context={"form": form})


def privacy_policy(request):
    return render(request, "pages/privacy_policy.html")


def terms_of_service(request):
    return render(request, "pages/terms_of_service.html")

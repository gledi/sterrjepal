from django.contrib import admin

from .models import Vehicle, Make, VehicleModel, Picture, Profile


admin.site.register(Make)
admin.site.register(VehicleModel)


class PictureInline(admin.TabularInline):
    model = Picture
    max_num = 5


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    inlines = [PictureInline]


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass

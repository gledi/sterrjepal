from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Make(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class VehicleModel(models.Model):
    name = models.CharField(max_length=128)
    make = models.ForeignKey(Make, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Vehicle(models.Model):
    class TransmissionType(models.TextChoices):
        Automatic = "automatic", _("Automatic")
        Manual = "manual", _("Manual")

    title = models.CharField(_("title"), max_length=256)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    year = models.IntegerField(_("year"))
    transmission = models.CharField(max_length=16, choices=TransmissionType.choices)
    km = models.IntegerField(blank=True, null=True)
    color = models.CharField(max_length=64)
    description = models.TextField(blank=True)
    make = models.ForeignKey(Make, null=True, on_delete=models.SET_NULL)
    vehicle_model = models.ForeignKey(VehicleModel, null=True, on_delete=models.SET_NULL)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    published_on = models.DateTimeField()

    def __str__(self):
        return self.title


class Picture(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to="pictures")


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=32)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=64)
    country = models.CharField(max_length=64)
    zip_code = models.CharField(max_length=16)
    picture = models.ImageField(upload_to="profile_pictures", blank=True, null=True)
    bio = models.TextField(blank=True)

    def __str__(self):
        return self.user.get_full_name()

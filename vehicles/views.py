from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest, HttpResponse

from .models import Vehicle


def get_vehicle_list(request: HttpRequest) -> HttpResponse:
    vehicles = Vehicle.objects.order_by('-published_on').all()

    return render(request, "vehicles/vehicle_list.html", {
        "vehicles": vehicles
    })


def get_vehicle_details(request: HttpRequest, pk: int) -> HttpResponse:
    vehicle = get_object_or_404(Vehicle.objects.select_related('make', 'vehicle_model'), pk=pk)
    return render(request, "vehicles/vehicle_detail.html", {
        "vehicle": vehicle,
    })

from django.urls import path

from .views import get_vehicle_list, get_vehicle_details

urlpatterns = [
    path("", get_vehicle_list, name="vehicle_list"),
    path("<int:pk>/", get_vehicle_details, name="vehicle_detail"),
]
